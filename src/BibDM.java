import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
public class BibDM{

    /**
     * Ajoute deux entiers
     * @param a le premier entier à ajouter
     * @param b le deuxieme entier à ajouter
     * @return la somme des deux entiers
     */
    public static Integer plus(Integer a, Integer b){
        return a+b;
    }


    /**
     * Renvoie la valeur du plus petit élément d'une liste d'entiers
     * VOUS DEVEZ LA CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
     * @param liste
     * @return le plus petit élément de liste
     */
    public static Integer min(List<Integer> liste){
        if (liste.size() == 0)
            return null;
        Integer val_min = liste.get(0);
        for (int i=1;i<liste.size();i++){
            if (liste.get(i)<val_min)
                val_min = liste.get(i);
        }
        return val_min;
    }
    
    
    /**
     * Teste si tous les élements d'une liste sont plus petits qu'une valeure donnée
     * @param valeur 
     * @param liste
     * @return true si tous les elements de liste sont plus grands que valeur.
     */
    public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T valeur , List<T> liste){
        for (T elem:liste){
            if (valeur.compareTo(elem) >= 0)
                return false;
        }
        return true;
    }



    /**
     * Intersection de deux listes données par ordre croissant.
     * @param liste1 une liste triée
     * @param liste2 une liste triée
     * @return une liste triée avec les éléments communs à liste1 et liste2
     */
    public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2){
        List<T> liste = new ArrayList<>();

        if (liste1.size() !=0 && liste2.size() !=0 && liste1.get(liste1.size()-1).compareTo(liste2.get(0)) < 0)
            return liste;

        for (T elem:liste1){
            if (liste2.contains(elem))
                if (!liste.contains(elem))
                    liste.add(elem);
        }
        return liste;
    }


    /**
     * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
     * @param texte une chaine de caractères
     * @return une liste de mots, correspondant aux mots de texte.
     */
    public static List<String> decoupe(String texte){
        
        List<String> liste = new ArrayList<>();
        if (texte.isEmpty())
            return liste;
        String mot="";
        for (int i=0;i<texte.length();i++){
            char c = texte.charAt(i);
            if (c != ' ')
                mot += c;
            if (c == ' ' || i == texte.length()-1){
                if (mot != "")
                    liste.add(mot);
                    mot = "";
            }
        }
        return liste;
    }


    /**
     * Renvoie le mot le plus présent dans un texte.
     * @param texte une chaine de caractères
     * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
     */

    public static String motMajoritaire(String texte){
        if (texte.isEmpty())
            return null;
        int max = 0;
        String smax = null;
        List<String> liste = decoupe(texte);
        List<String> mot_vus = new ArrayList<>();
        for (String mot:liste){
            if (!mot_vus.contains(mot)){
                int f = Collections.frequency(liste, mot);
                if (f > max){
                    max = f;
                    smax = mot;
                }
                if (f == max){
                    if (smax.compareTo(mot) > 0 )
                        smax = mot;
                }
                mot_vus.add(mot);
            }
        }

        return smax;
    }
    
    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de ( et de )
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
     */
    public static boolean bienParenthesee(String chaine){
        int cpt = 0;
        for (int i=0;i<chaine.length();i++){
            char c = chaine.charAt(i);
            if (c == '(')
                cpt ++;
            if (c == ')'){
                if (cpt == 0)
                    return false;
                cpt --;
            }
        }
        return cpt == 0;
    }
    
    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors ue ([]) est bien parenthèsée.
     */
    public static boolean bienParentheseeCrochets(String chaine){
        String l = "";
        for (int i=0;i<chaine.length();i++){
            Character c = chaine.charAt(i);
            if (c == '(')
                l += ")";
            if (c == '[')
                l += "]";
            if (c == ')' || c == ']'){
                if (l.isEmpty() || l.charAt(l.length()-1) != c )
                    return false;
                l = l.substring(0, l.length()-1);
            }
        }
        return l.isEmpty();

    }


    /**
     * Recherche par dichtomie d'un élément dans une liste triée
     * @param liste, une liste triée d'entiers
     * @param valeur un entier
     * @return true si l'entier appartient à la liste.
     */
    public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur){
        
        int ifin, id, imilieu;

        ifin = liste.size()-1;
        id = 0;

        while (id <= ifin){
            imilieu = (id + ifin)/2;
            int i = liste.get(imilieu).compareTo(valeur);
            if (i == 0)
                return true;
            if (i > 0)
                ifin = imilieu - 1;
            if (i < 0 )
                id = imilieu + 1;
        }

        return false;
    }



}
